FROM node:latest

# Create the directory!
RUN mkdir -p /usr/src/mathiasshaw
WORKDIR /usr/src/mathiasshaw

# Copy and Install our bot
COPY package.json /usr/src/mathiasshaw
RUN npm install

# Our precious bot
COPY . /usr/src/mathiasshaw

# Start me!
CMD ["node", "index.js"]
