const Discord = require("discord.js");
const client  = new Discord.Client();
const auth    = require("./settings.json");
const request = require("request");

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setActivity("Votre personnage sur WoW !", {
        type: "WATCHING"
    });
});

function pp (obj) { return JSON.stringify (obj, undefined, 2); }

const wow_class_colors = {
		"Death Knight" : 12853051,
		"Demon Hunter" : 10694857,
		"Druid"        : 16743690,
		"Hunter"       : 11261043,
		"Mage"         : 6933744,
		"Monk"         : 65430,
		"Paladin"      : 16092346,
		"Priest"       : 16777215,
		"Rogue"        : 16774505,
		"Shaman"       : 28894,
		"Warlock"      : 9732809,
		"Warrior"      : 13081710
}

client.on("message", msg => {
 
    if (msg.author.bot) return;
		if (!msg.content.startsWith('!')) return;
    

    var cmd = msg.content.trim().split(" ");
    if (cmd.length < 1) { // Impossible selon moi mais bon
        msg.channel.send ("ERREUR: Message vide ??? \n" + msg.content);
        return;
    }

    function check_cmd_format (op, format) {
        if (op.length != format.trim().split(' ').length) {
            msg.channel.send(`ERREUR: Format attendu : ${format}`);
            msg.channel.send(`Note: Nous regardons seulement les serveurs EU.`);
            return false;
        }
        return true;
    }

    switch (cmd[0].trim ()) {
        case "!ping": {
            if (check_cmd_format (cmd, "!ping")) {
                msg.reply("Pong!");
            }
            break;
        }
        case "!score": {
            if (check_cmd_format (cmd, "!score Pseudo Region")) {
                request (make_raider_request (cmd[2], cmd[1], cmd[0]), 
                    make_handle_request (cmd[0]));
            }
            break;
        }
        case "!curved": {
            if (check_cmd_format (cmd, "!curved Pseudo Region")) {
                request (make_raider_request (cmd[2], cmd[1], cmd[0]), 
                    make_handle_request (cmd[0]));
            }
            break;
        }
        case "!key": {
            if (check_cmd_format (cmd, "!key Pseudo Region")) {
                request (make_raider_request (cmd[2], cmd[1], cmd[0]), 
                    make_handle_request (cmd[0]));
            }
            break;
        }
        case "!colors": {
            if (check_cmd_format (cmd, "!colors")) {
                for (let [wow_class, color] of Object.entries(wow_class_colors)) {
                    let c = make_simple_embed_msg (`Class: ${wow_class}`, `Color: ${color}`, color);
                    msg.channel.send (c);
								}
            }
            break;
        }
        default: {
            msg.channel.send("ERREUR: Commande non-reconnue (" + cmd[0] + ").");

            break;
        }
    }

    function make_handle_request (cmd) {
        return function (error, response, body) {
            if (error || response.statusCode !== 200) {
                msg.reply("There was an error with the raider.io request, please try again.");
                console.log ("ERROR : raider.io failure");
                console.log (`ERROR : raider.io error='${pp(error)}'`);
                console.log (`ERROR : raider.io response='${pp(response)}'`);
                return;
            }
            switch (cmd) {
                case "!score":
                    msg.reply (parse_request_score (body));
                    break;
                case "!profile":
                    msg.reply (parse_request_profile (body));
                    break;
                case "!curved":
                    msg.reply (parse_request_curved (body));
                    break;
                case "!key":
                    msg.reply (parse_request_key (body));
                    break;
            }
        };
    }
});

function make_raider_request (realm, name, cmd) {
    let req_raiderio_profile = new URL("/api/v1/characters/profile", "http://raider.io");

    req_raiderio_profile.searchParams.append ("region", "eu");
    req_raiderio_profile.searchParams.append ("realm" , realm);
    req_raiderio_profile.searchParams.append ("name"  , name);
    switch (cmd) {
        case "!score":
            req_raiderio_profile.searchParams.append ("fields" , "mythic_plus_scores_by_season:current,guild,covenant");
            break;
        case "!profile":
            req_raiderio_profile.searchParams.append ("fields" , "mythic_plus_scores_by_season:current,guild,covenant");
            break;
        case "!curved":
            req_raiderio_profile.searchParams.append ("fields" , "raid_achievement_curve:castle-nathria");
            break;
        case "!key":
            req_raiderio_profile.searchParams.append ("fields" , "mythic_plus_highest_level_runs,mythic_plus_weekly_highest_level_runs,mythic_plus_previous_weekly_highest_level_runs");
            break;
    }

    return req_raiderio_profile.href;
}

function make_simple_embed_msg (title, desc, color) {
		let msg = new Discord.MessageEmbed ();
		if (title) msg.setTitle (`__**${title}**__`);
		if (desc)  msg.setDescription (desc);
		if (color)
				msg.setColor (color) 
		else
				msg.setColor ("DEFAULT");
					
		return msg;
}


function make_template_msg (json) {
 		let msg = new Discord.MessageEmbed ();
		msg.setTitle (`__**${json.name}-${json.realm}**__`);
    msg.setFooter ("MAJ: 9.0.1", "https://vignette.wikia.nocookie.net/wowwiki/images/c/c2/Shadowlandsbutton.png");
    msg.setThumbnail (json.thumbnail_url);
    msg.setColor (wow_class_colors[json.class]);
    return msg;
}

function parse_request_profile (body) {
    let json = JSON.parse(body);

    let allScore  = json.mythic_plus_scores_by_season[0].scores.all;
    let healScore = json.mythic_plus_scores_by_season[0].scores.healer;
    let dpsScore  = json.mythic_plus_scores_by_season[0].scores.dps;
    let tankScore = json.mythic_plus_scores_by_season[0].scores.tank;

    let msg_reply = make_template_msg (json);
    msg_reply.addField ("__**Infos**__", 
        "**Pseudo: **"     + json.name + "\n" +
        "**Serveur:** "    + json.realm + "\n" +
        "**Guilde:** "     + json.guild.name + "\n" + 
        "**Race: **"       + json.race + "\n" + 
        "**Classe: **"     + json.class + "\n" +
        "**Spe active: **" + json.active_spec_name + "\n" + 
        "**Faction: **"    + json.faction + "\n");
    msg_reply.addField ("__Covenant__",
        "**Covenant: **"  + json.covenant.name + "(" + json.covenant.renown_level + ")\n");
    msg_reply.addField ("__SCORES MM+__",
        ":shield: "            + tankScore + "\n" + 
        ":crossed_swords: "    + dpsScore + "\n" +
        ":helmet_with_cross: " + healScore + "\n" +
        "Total: " + allScore + "\n");
    msg_reply.addField ("__**Liens**__", 
        "**Armurie: **" + "https://worldofwarcraft.com/fr-fr/character/eu/" + json.realm + "/" + json.name + "\n" + 
        "**Raider.io: **" + json.profile_url + "\n" + 
        "**WarcraftLogs: **https://www.warcraftlogs.com/character/eu/" + json.realm + "/" + json.name + "\n" + 
        "**WoWAnalyzer: **https://wowanalyzer.com/character/EU/" + json.realm + "/" + json.name + "\n" + 
        "**WoWProgress: **https://www.wowprogress.com/character/eu/" + json.realm + "/" + json.name + "\n");
    return msg_reply;
}

function parse_request_score (body) {
    let json = JSON.parse(body);

    let allScore  = json.mythic_plus_scores_by_season[0].scores.all;
    let healScore = json.mythic_plus_scores_by_season[0].scores.healer;
    let dpsScore  = json.mythic_plus_scores_by_season[0].scores.dps;
    let tankScore = json.mythic_plus_scores_by_season[0].scores.tank;

    let msg_reply = make_template_msg (json);
    msg_reply.addField ("__**Infos**__", 
        "**Pseudo: **"     + json.name + "\n" +
        "**Serveur:** "    + json.realm + "\n" +
        "**Guilde:** "     + json.guild.name + "\n" + 
        "**Race: **"       + json.race + "\n" + 
        "**Classe: **"     + json.class + "\n" +
        "**Spe active: **" + json.active_spec_name + "\n" + 
        "**Faction: **"    + json.faction + "\n");
    msg_reply.addField ("__Covenant__",
        "**Covenant: **"  + json.covenant.name + "(" + json.covenant.renown_level + ")\n");
    msg_reply.addField ("__SCORES MM+__",
        ":shield: "            + tankScore + "\n" + 
        ":crossed_swords: "    + dpsScore + "\n" +
        ":helmet_with_cross: " + healScore + "\n" +
        "Total: " + allScore + "\n");
    msg_reply.addField ("__**Lien**__", 
        "**Raider.io: **" + json.profile_url + "\n");
    return msg_reply;
} 

function parse_request_curved (body) {
    let json = JSON.parse(body);

    let curved = (json.raid_achievement_curve.length == 0) ? ":red_circle:" : ":green_circle:";
    
    let msg_reply = make_template_msg (json);
    msg_reply.addField ("__**Curved**__", 
      "**Pseudo: **" + json.name + "\n" +
      "**Serveur:** " + json.realm + "\n" +
      "**Curved: **" + curved + "\n");
    return msg_reply;
}

function parse_request_key (body) {
    let json = JSON.parse(body);

    function eval_mythic (mythic_array) {
        if (mythic_array.length == 0) {
            return ":red_circle: N/A";
        }
        let top_key     = mythic_array[0].mythic_level;
        let top_dungeon = mythic_array[0].dungeon;
        return ":green_circle: ( +" + top_key + ") ( " + top_dungeon + ")";
    }

    let mmplus    = eval_mythic (json.mythic_plus_weekly_highest_level_runs);
    let lwmmplus  = eval_mythic (json.mythic_plus_previous_weekly_highest_level_runs);
    let topmmplus = eval_mythic (json.mythic_plus_highest_level_runs);

    let msg_reply = make_template_msg (json);
    msg_reply.addField ("__**Mythic plus meilleure clef de la saison**__", 
      "\n**" + topmmplus + "**\n");
    msg_reply.addField ("__**Mythic plus de la semaine**__", 
      "\n**" + mmplus + "**\n");
    msg_reply.addField ("__**Mythic plus de la semaine précedente**__", 
      "\n**" + lwmmplus + "**\n");

    return msg_reply;
}

client.login(auth.token);
